import { NodeBlock, validator } from "tripetto-runner-foundation";

export abstract class Error extends NodeBlock {
    @validator
    stopHere(): boolean {
        return false;
    }
}
